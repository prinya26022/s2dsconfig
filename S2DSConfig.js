"use strict";

var S2DSConfig = SAGE2_App.extend({
    init: async function (data) {
        if (this.isElectron()) {
            this.SAGE2Init("webview", data);
            this.createLayer("rgba(0,0,0,0.85)");
            this.layer.style.overflow = "hidden";
            this.pre = document.createElement('pre');
            this.pre.style.whiteSpace = "pre-wrap";
            this.layer.appendChild(this.pre);
            this.console = false;
        } else {
            this.SAGE2Init("div", data);
            this.element.innerHTML = "<h1>Webview only supported using Electron as a display client</h1>";
            return;
        }
        // Set the DOM id
        this.element.id = "div_" + data.id;
        this.appId = data.id;
        //this.element.style.backgroundColor = "white"; 

        // move and resize callbacks
        this.resizeEvents = "continuous";
        this.modifiers = [];
        this.contentType = "web";
        // make HTML fit the screen
        this.element.style.display = "inline-flex";

        // Webview settings
        this.element.autosize = "on";
        this.element.plugins = "on";
        this.element.allowpopups = false;
        this.element.allowfullscreen = false;
        // turn off nodejs intergration for now
        this.element.nodeintegration = 0;
        // add the preload clause
        this.addPreloadFile();
        // initial size
        this.element.minwidth = data.width;
        this.element.minheight = data.height;

        // Set a session per webview, so not zoom sharing per origin
        this.element.partition = data.id;
        this.zoomFactor = 1;
        this.position_x = 0;
        this.position_y = 0;
        this.myChild = [];

        this.countConfig = 0;
        this.dataConfig = await this.readFile();

        //ให้ S2DS อ่าน all config แทน
        // this.configArray = await this.readAllconfig();

        // Auto-refresh time
        this.autoRefresh = null;
        var _this = this;

        var path = require("path");
        // access the remote electron process
        var app = require("electron").remote.app;
        // get the application path
        var appPath = app.getAppPath();
        // split the path at node_modules
        var subPath = appPath.split("node_modules");
        // take the first element which contains the current folder of the application
        var rootPath = subPath[0];
        // add the relative path to the webview folder
        var preloadPath = path.join(rootPath, 'public/uploads/apps/S2DSConfig/UI', 'flowchart.html');
        

        this.element.src = preloadPath;
        
        //-------------------------------------------------------------------------------------------------------------------------
        // EventListener

        // done loading
        // this.element.addEventListener("did-finish-load", function() {
        this.element.addEventListener("did-stop-loading", function () {
            // code injection to support key translation
            _this.codeInject();

            // update the context menu with the current URL
            _this.getFullContextMenuAndUpdate();
            _this.isLoading = false;
            // recalculate the scaling
            _this.resize();
            _this.messageDelay = 0;
            _this.messageHead = "";
            _this.attribute = "";
        });

        this.element.addEventListener("did-fail-load", function (event) {
            console.log('Webview> Did fail load', event);
            // not loading anymore
            _this.isLoading = false;
            // Check the return code
            if (event.errorCode === -3 ||
                event.errorCode === -27 ||
                event.errorDescription === "OK") {
                // it's a redirect (causes issues)
                // _this.changeURL(event.validatedURL, true);
            } else if (event.errorCode === -501) {
                // Add the message to the console layer
                _this.pre.innerHTML += 'Webview> certificate error:' + event + '\n';
                _this.element.src = 'data:text/html;charset=utf-8,' +
                    '<h1>This webpage has invalid certificates and cannot be loaded</h1>';
                _this.changeWebviewTitle();
            } else {
                // real error
                _this.element.src = 'data:text/html;charset=utf-8,<h1>Invalid URL</h1>';
                _this.changeWebviewTitle();
            }
        });

        // When the page request fullscreen
        this.element.addEventListener("enter-html-full-screen", function (event) {
            console.log('Webview>	Enter fullscreen');
            // not sure if this works
            event.preventDefault();
        });
        this.element.addEventListener("leave-html-full-screen", function (event) {
            console.log('Webview>	Leave fullscreen');
            // not sure if this works
            event.preventDefault();
        });

        // Console message from the embedded page
        this.element.addEventListener('console-message', function (event) {
            console.log('Webview>	console:', event.message);
            // Add the message to the console layer
            // _this.pre.innerHTML += 'Webview> ' + event.message + '\n';
            
            let res;
            try {
                const fs = require("fs");
                res = event.message.split("#");

                
                
                if (res[0] == "writefilesection") {
                    var userlist = res[1].split(",_,")
                    console.log(userlist)
                    var dashboardId = "config_" + userlist[0].substring(10);

                    fs.writeFile("public/uploads/apps/S2DS/Config_S2DS/config.txt", res[1], function (err) {
                        if (err) {
                            return console.log(err);
                        }
                        console.log("config.txt was saved!");
                        fs.writeFile("public/uploads/apps/S2DS/Config_S2DS/" + dashboardId + ".txt", res[1], function (err) {
                            if (err) {
                                return console.log(err);
                            }
                            console.log("" + dashboardId + " was saved!");
                        });
                    });

                }
                if (res[0] == "defaultwritefilesection") {
                    var workflow = "logworkflow_" + res[1];
                    fs.writeFile("public/uploads/apps/S2DS/logworkflow.txt", res[2], function (err) {
                        if (err) {
                            return console.log(err);
                        }
                        console.log("logworkflow was saved!");
                        fs.writeFile("public/uploads/apps/S2DS/" + workflow + ".txt", res[2], function (err) {
                            if (err) {
                                return console.log(err);
                            }
                            console.log(workflow + " logworkflow was saved!");
                        });
                    });

                }
            } catch (error) { }


        });

        // When the webview tries to open a new window, for insance with ALT-click
        this.element.addEventListener("new-window", function (event) {
            // only accept http protocols
            if (event.url.startsWith('http:') || event.url.startsWith('https:')) {
                // Do not open a new view, just navigate to the new URL
                // _this.changeURL(event.url, true);
                // calculate a position right next to the parent view
                let pos = [_this.sage2_x + _this.sage2_width + 5,
                _this.sage2_y - _this.config.ui.titleBarHeight];
                // Check if the horizontal position is too close to the side
                if ((_this.config.totalWidth - pos[0]) < 100) {
                    // shift to the left
                    pos[0] = _this.config.totalWidth - _this.sage2_width;
                }
                // Check if it's a PDF
                console.log('new-window', event.url);
                if (isMaster) {
                    // Request a new webview application
                    wsio.emit('openNewWebpage', {
                        // should be uniqueID, but no interactor object here
                        id: this.id,
                        // send the new URL
                        url: event.url,
                        // position to the left
                        position: pos
                    });
                }
            } else {
                console.log('Webview>	Not a HTTP URL, not opening [', event.url, ']', event);
            }
            // clear the modifiers array (get sticky keys otherwise)
            _this.modifiers = [];
        });
    },

    //------------------------------------------------------------------------------------------------------------------------------

    /**
     * Determines if electron is the renderer (instead of a browser)
     *
     * @method     isElectron
     * @return     {Boolean}  True if electron, False otherwise.
     */
    isElectron: function () {
        return (typeof window !== 'undefined' && window.process && window.process.type === "renderer");
    },

    /**
     * Loads the components to do a file preload on a webpage.
     * Needs to be within an Electron browser to work.
     *
     * @method     addPreloadFile
     */
    addPreloadFile: function () {
        // if it's not running inside Electron, do not bother
        if (!this.isElectron) {
            return;
        }
        // load the nodejs path module
        var path = require("path");
        // access the remote electron process
        var app = require("electron").remote.app;
        // get the application path
        var appPath = app.getAppPath();
        // split the path at node_modules
        var subPath = appPath.split("node_modules");
        // take the first element which contains the current folder of the application
        var rootPath = subPath[0];
        // add the relative path to the webview folder
        var preloadPath = path.join(rootPath, 'public/uploads/apps/S2DSConfig', 'SAGE2_script_supplement.js');
        // finally make it a local URL and pass it to the webview element
        this.element.preload = "file://" + preloadPath;

    },

    /**
     * Reload the content of the webview
     *
     * @method     reloadPage
     * @param      {Object}  responseObject  if time parameter passed, used as a timer
     */
    reloadPage: function (responseObject) {
        if (this.isElectron()) {
            if (responseObject.time) {
                // if an argument passed, use it for timer
                if (isMaster) {
                    // Parse the value we got
                    var interval = parseInt(responseObject.time, 10) * 1000;
                    var _this = this;
                    // build the timer
                    this.autoRefresh = setInterval(function () {
                        // send the message to the server to relay
                        _this.broadcast("reloadPage", {});
                    }, interval);
                    // change the title to add the spinner
                    this.changeWebviewTitle();
                }
            } else {
                // Just reload once
                this.isLoading = true;
                this.element.reload();
                this.element.getWebContents().zoomFactor = this.state.zoom;
            }
        }
    },

    /**
     * Change the title of the window
     *
     * @method     changeWebviewTitle
     * @param newtitle {String} new title
     */
    changeWebviewTitle: function (newtitle) {
        if (newtitle) {
            // if parameter passed, we update the title
            this.title = 'Webview: ' + newtitle;
        }
        var newtext = this.title;
        // if the page has a reload timer
        if (this.autoRefresh) {
            // add a timer using a FontAwsome character
            newtext += ' <i class="fa">\u{f017}</i>';
        }
        // if the page is loading
        if (this.isLoading && this.contentType === "web") {
            // add a spinner using a FontAwsome character
            newtext += ' <i class="fa fa-spinner fa-spin"></i>';
        }
        // call the base class method
        this.updateTitle(newtext);
    },

    load: function (date) {
        // sync the change
        this.mainWebView.src = this.state.url;
        this.refresh(date);
    },

    resize: function (date) {
        // Called when window is resized
        this.element.style.width = this.sage2_width + "px";
        this.element.style.height = this.sage2_height + "px";

        // resize the console layer
        if (this.layer) {
            // make sure the layer exist first
            this.layer.style.width = this.element.style.width;
            this.layer.style.height = this.element.style.height;
        }

        if (this.contentType === "web") {
            let content = this.element.getWebContents();
            let ar = this.sage2_width / this.sage2_height;
            if (ar >= 1.0) {
                // landscape window
                let scale = this.sage2_width / 1440;
                if (scale < 1.2) {
                    content.enableDeviceEmulation({
                        screenPosition: "desktop",
                        deviceScaleFactor: 0
                    });
                } else {
                    content.enableDeviceEmulation({
                        screenSize: {
                            width: this.sage2_width,
                            height: this.sage2_height
                        },
                        viewSize: {
                            width: this.sage2_width * scale,
                            height: this.sage2_height * scale
                        },
                        scale: scale,
                        deviceScaleFactor: 0
                    });
                }
            } else {
                // portrait window
                let scale = this.sage2_height / 1440;
                if (scale < 1.2) {
                    content.enableDeviceEmulation({
                        screenPosition: "desktop",
                        deviceScaleFactor: 0
                    });
                } else {
                    content.enableDeviceEmulation({
                        screenPosition: "mobile",
                        screenSize: {
                            width: this.sage2_width,
                            height: this.sage2_height
                        },
                        viewSize: {
                            width: this.sage2_width / scale,
                            height: this.sage2_height / scale
                        },
                        scale: scale,
                        deviceScaleFactor: 0
                    });
                }
            }
        }

        this.refresh(date);
    },

    draw: function (date) {
    },

    changeURL: function (newlocation, remoteSync) {
        // trigger the change
        this.element.src = newlocation; //this.addSessionAsUrlParamIfConnectingToSelf(newlocation);
        // save the url
        this.state.url = newlocation;
        this.SAGE2Sync(remoteSync);
    },

    quit: function () {
        if (this.autoRefresh) {
            // cancel the autoreload timer
            clearInterval(this.autoRefresh);
        }
    },

    zoomPage: function (responseObject) {
        if (this.isElectron()) {
            var dir = responseObject.dir;

            // zoomin
            if (dir === "zoomin") {
                this.state.zoom *= 1.50;
                this.element.getWebContents().zoomFactor = this.state.zoom;
            }

            // zoomout
            if (dir === "zoomout") {
                this.state.zoom /= 1.50;
                this.element.getWebContents().zoomFactor = this.state.zoom;
            }

            this.refresh();
        }
    },

    changeMode: function (responseObject) {
        if (this.isElectron()) {
            this.state.mode = responseObject.mode;
            this.updateMode();
            this.refresh();
        }
    },

    event: function (eventType, position, user_id, data, date) {
        if (this.isElectron()) {
            // Making Integer values, seems to be required by sendInputEvent
            var x = Math.round(position.x);
            var y = Math.round(position.y);
            var _this = this;

            if (eventType === "pointerPress") {
                // click
                this.element.sendInputEvent({
                    type: "mouseDown",
                    x: x, y: y,
                    button: data.button,
                    modifiers: this.modifiers,
                    clickCount: 1
                });
            }
            else if (eventType === "pointerMove") {
                // move
                this.element.sendInputEvent({
                    type: "mouseMove",
                    modifiers: this.modifiers,
                    x: x, y: y
                });
            } else if (eventType === "pointerRelease") {
                // click release
                this.element.sendInputEvent({
                    type: "mouseUp",
                    x: x, y: y,
                    button: data.button,
                    modifiers: this.modifiers,
                    clickCount: 1
                });
            } else if (eventType === "pointerScroll") {
                // Scroll events: reverse the amount to get correct direction
                this.element.sendInputEvent({
                    type: "mouseWheel",
                    deltaX: 0, deltaY: -1 * data.wheelDelta,
                    x: x, y: y,
                    modifiers: this.modifiers,
                    canScroll: true
                });
            }
            else if (eventType === "widgetEvent") {
                // widget events
            } else if (eventType === "keyboard") {
                // send the character event
                this.simulateChar({ key: data.character });
                setTimeout(function () {
                    _this.element.sendInputEvent({
                        type: "keyUp",
                        keyCode: data.character
                    });
                }, 0);
            } else if (eventType === "specialKey") {
                // clear the array
                this.modifiers = [];
                // store the modifiers values
                if (data.status && data.status.SHIFT) {
                    this.modifiers.push("shift");
                }
                if (data.status && data.status.CTRL) {
                    this.modifiers.push("control");
                }
                if (data.status && data.status.ALT) {
                    this.modifiers.push("alt");
                }
                if (data.status && data.status.CMD) {
                    this.modifiers.push("meta");
                }
                if (data.status && data.status.CAPS) {
                    this.modifiers.push("capsLock");
                }

                // SHIFT key
                if (data.code === 16) {
                    if (data.state === "down") {
                        this.element.sendInputEvent({
                            type: "keyDown",
                            keyCode: "Shift"
                        });
                    } else {
                        this.element.sendInputEvent({
                            type: "keyUp",
                            keyCode: "Shift"
                        });
                    }
                }
                // backspace key
                if (data.code === 8 || data.code === 46) {
                    if (data.state === "down") {
                        // The delete is too quick potentially.
                        // Currently only allow on keyup have finer control
                    } else {
                        this.element.sendInputEvent({
                            type: "keyUp",
                            keyCode: "Backspace"
                        });
                    }
                }

                if (data.code === 37 && data.state === "down") {
                    // arrow left
                    if (data.status.ALT) {
                        // navigate back
                        this.element.goBack();
                    } else {
                        // send the left arrow key
                        this.element.sendInputEvent({
                            type: "keyDown",
                            keyCode: "Left",
                            modifiers: null
                        });
                    }
                    this.refresh(date);
                } else if (data.code === 38 && data.state === "down") {
                    // arrow up
                    if (data.status.ALT) {
                        // ALT-up_arrow zooms in
                        this.zoomPage({ dir: "zoomin" });
                    } else {
                        this.element.sendInputEvent({
                            type: "keyDown",
                            keyCode: "Up",
                            modifiers: null
                        });
                    }
                    this.refresh(date);
                } else if (data.code === 82 && data.state === "down") {
                    // r key
                    if (data.status.ALT) {
                        // ALT-r reloads
                        this.isLoading = true;
                        this.reloadPage({});
                    }
                    this.refresh(date);
                } else if (data.code === 39 && data.state === "down") {
                    // arrow right
                    if (data.status.ALT) {
                        // navigate forward
                        this.element.goForward();
                    } else {
                        // send the right arrow key
                        this.element.sendInputEvent({
                            type: "keyDown",
                            keyCode: "Right",
                            modifiers: null
                        });
                    }
                    this.refresh(date);
                } else if (data.code === 40 && data.state === "down") {
                    // arrow down
                    if (data.status.ALT) {
                        // ALT-down_arrow zooms out
                        this.zoomPage({ dir: "zoomout" });
                    } else {
                        this.element.sendInputEvent({
                            type: "keyDown",
                            keyCode: "Down",
                            modifiers: null
                        });
                    }
                    this.refresh(date);
                } else if (data.code === 86 && data.state === "down") {
                    // CTRL/CMD v for 'paste'
                    if (data.status.CTRL || data.status.CMD) {
                        // paste the content of the clipboard into the webview
                        this.element.paste();
                    }
                    this.refresh(date);
                }
            }
        }
    },

    getContextEntries: function () {
        var entries = [];
        var entry;



        entries.push({ description: "separator" });

        for (let i = 0; i < this.countConfig; i++) {
            console.log(this.countConfig)
            entry = {};
            entry.description = this.configArray[i];
            entry.callback = "changeConfig";
            entry.parameters = {};
            entry.parameters.configSelect = this.configArray[i];
            entries.push(entry);
        }





        entries.push({
            description: "Copy URL to Clipboard",
            callback: "SAGE2_copyURL",
            parameters: {
                url: this.state.url
            }
        });

        return entries;
    },



    //------------------------------------------------------------------------------------------------------------------------------
    // Working Here!



    // changeConfig: function (responseObject) {
    //         var configSelect = responseObject.configSelect;
    //         // load the nodejs path module
    //         var path = require("path");
    //         // access the remote electron process
    //         var app = require("electron").remote.app;
    //         // get the application path
    //         var appPath = app.getAppPath();
    //         // split the path at node_modules
    //         var subPath = appPath.split("node_modules");
    //         // take the first element which contains the current folder of the application
    //         var rootPath = subPath[0];
    //         // add the relative path to the webview folder
    //         var preloadPath = path.join(rootPath, 'S2DS_config', configSelect);

    //         var fs = require("fs");
    //         var data = fs.readFileSync(preloadPath);
    //         console.log(data.toString());

    // },


    readFile: function () {
        // if it's not running inside Electron, do not bother
        if (!this.isElectron) {
            return;
        }
        // load the nodejs path module
        var path = require("path");
        // access the remote electron process
        var app = require("electron").remote.app;
        // get the application path
        var appPath = app.getAppPath();
        // split the path at node_modules
        var subPath = appPath.split("node_modules");
        // take the first element which contains the current folder of the application
        var rootPath = subPath[0];
        // add the relative path to the webview folder
        var preloadPath = path.join(rootPath, 'public/uploads/apps/S2DS', 'logworkflow.txt');
        // this.configSet = path.join(rootPath, 'public/uploads/apps/S2DS/UI', 'flowchart.html');

        const fs = require("fs");
        // Synchronous read
        var data = fs.readFileSync(preloadPath);



        return data;
    },

    readAllconfig: function () {
        if (!this.isElectron) {
            return;
        }
        // load the nodejs path module
        var path = require("path");
        // access the remote electron process
        var app = require("electron").remote.app;
        // get the application path
        var appPath = app.getAppPath();
        // split the path at node_modules
        var subPath = appPath.split("node_modules");
        // take the first element which contains the current folder of the application
        var rootPath = subPath[0];

        var allconfigPath = path.join(rootPath, 'S2DS_config');

        var fs = require("fs");
        // Synchronous read

        var configArray = [];
        const files = fs.readdirSync(allconfigPath);
        for (const file of files) {
            if (file.split(".")[1] == "txt") {
                console.log(file)
                this.countConfig++;
                configArray.push(file);

            }

        }

        return configArray;
    },




    codeInject: function () {
        var tmp = this.dataConfig + "";

        // console.log(tmp);


        this.element.executeJavaScript(
            'test = document.querySelector("#mySavedModel");'
            // +'console.log(test.value = '+tmp+');'
            + 'test.value = ' + JSON.stringify(tmp, undefined, 4) + ';'
            + 'document.querySelector("#LoadButton").click();'

        )




    },
    // test.value = `+JSON.stringify(tmp).toString()+`;`

})