window.onload = function () {
	document.getElementById("mySavedModel").value = localStorage.logData;
}

function download(data, filename, type) {
	var file = new Blob([data], { type: type });
	if (window.navigator.msSaveOrOpenBlob) // IE10+
		window.navigator.msSaveOrOpenBlob(file, filename);
	else { // Others
		var a = document.createElement("a"),
			url = URL.createObjectURL(file);
		a.href = url;
		a.download = filename;
		document.body.appendChild(a);
		a.click();
		setTimeout(function () {
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);
		}, 0);
	}
}
function FileSave(sourceText, fileIdentity) {
	var workElement = document.createElement("a");
	if ('download' in workElement) {
		workElement.href = "data:" + 'text/plain' + "charset=utf-8," + escape(sourceText);
		workElement.setAttribute("download", fileIdentity);
		document.body.appendChild(workElement);
		var eventMouse = document.createEvent("MouseEvents");
		eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		workElement.dispatchEvent(eventMouse);
		document.body.removeChild(workElement);
	} else throw 'File saving not supported for this browser';
}

function getfromSaveAs() {

	save()
	var checkArr = [];
	var dashboardID_check;
	var page_arr_check = [];
	var link_arr_check = [];
	var dataStudioWebID;
	var check_dashbord_only_one = true;
	var data = document.getElementById("mySavedModel").value;
	var obj = JSON.parse(data);
	var configText = "dashboard:";
	try {
		for (let i = 0; i < obj.nodeDataArray.length; i++) {
			console.log(obj.nodeDataArray[i]);
			if (obj.nodeDataArray[i].category == "Comment" && (obj.nodeDataArray[i].text.split("-").length == 5 && check_dashbord_only_one == true)) {
				dataStudioWebID = obj.nodeDataArray[i].text;

				configText += dataStudioWebID + ",_,\n";
				//check dashboard ID
				dashboardID_check = dataStudioWebID;
				check_dashbord_only_one = false
			} else if (obj.nodeDataArray[i].category == "Comment" && (obj.nodeDataArray[i].text.split("-").length == 5 && check_dashbord_only_one == false)) {
				throw new Error("Please add only one Dashboard")
			}
			// dashboard

		}
		console.log(dataStudioWebID)
		console.log(dashboardID_check);

		if (dataStudioWebID === undefined) {
			throw new Error("Please check your Dashboard ID")
		}


		for (let i = 0; i < obj.nodeDataArray.length; i++) {
			if (obj.nodeDataArray[i].text.split(":").length == 2) {
				checkArr.push(obj.nodeDataArray[i].text);
			}

		}
		//find all page
		var check_mainpage = false
		if (checkArr.length == 0) {
			throw new Error("Please add Page of Dashboard or check format value in box Page again.")


		} else {


			var page_define = uniqueArray2(checkArr);
			page_define = sort_temp_check(page_define);
			console.log(page_define)
			for (let i = 0; i < page_define.length; i++) {
				if (/[p|P][0-9]+[/:]\w+$/.test(page_define[i]) == false) {
					throw new Error(page_define[i] + " wrong formate Please try again.")
				}
				if (page_define[i].split(":")[0] == "P1") {
					check_mainpage = true;
				}

				configText += page_define[i] + ",_,\n";
				//page define

				//check page
				page_arr_check.push(page_define[i])
			}
			if (check_mainpage == false) {
				throw new Error("P1(main page of dashboard) is missing Please check again.")
			}

			for (let i = 0; i < obj.nodeDataArray.length; i++) {
				if (obj.nodeDataArray[i].category == "Conditional") {
					configText += "link:"
					var arrSrc = findSrc(obj, obj.nodeDataArray[i].key);
					var arrDest = findDest(obj, obj.nodeDataArray[i].key);
					if (arrSrc.length <= 0) {
						throw new Error("Please add Source page before save.")
					}

					var param = obj.nodeDataArray[i].text;
					if (/\w+[\(]\w+[\)]$/.test(param) == false) {
						throw new Error(param + " wrong format Please try again.");
					}
					configText += arrSrc[0].split(":")[0] + "->" + param + "->[";

					if (arrDest.length <= 0) {
						throw new Error("Please add Destination page before save.")
					}
					for (let j = 0; j < arrDest.length; j++) {
						if (j == arrDest.length - 1) {
							configText += arrDest[j].split(":")[0];
						} else {
							configText += arrDest[j].split(":")[0] + "&"
						}

					}
					configText += "],_,\n"

				}
			}

			for (let i = 0; i < obj.nodeDataArray.length; i++) {
				if (obj.nodeDataArray[i].category == "Start") {
					configText += "map:"
					column_name_GoogleMaps = obj.nodeDataArray[i].text;
					console.log(column_name_GoogleMaps);

					var arrDes = findDest(obj, obj.nodeDataArray[i].key);
					console.log(arrDes);
					for (let j = 0; j < arrDest.length; j++) {
						// if(j == arrDest.length - 1){
						// 	configText += arrDest[j].split(":")[0];
						// } else {
						// 	configText += arrDest[j].split(":")[0] + "&"
						// }
						var tmp_page = arrDes[j].split(":")[0];
						configText += tmp_page + "->" + column_name_GoogleMaps;
					}

					configText += ",_,\n"
				}
			}


			var workflow_dashboard = configText.split(",_,")[0].substring(10)

			console.log("defaultwritefilesection#" + workflow_dashboard + "#" + data);
			download(configText, "config_" + dataStudioWebID, "text");
			closePopup()
			textInPopup(dashboardID_check, page_arr_check, configText)


		}

	} catch (e) {
		alert(e)
	}
}

function uniqueArray2(arr) {
	var a = [];
	for (var i = 0, l = arr.length; i < l; i++)
		if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
			a.push(arr[i]);
	return a;
}


function getfromSave() {

	save()
	var dashboardID_check;
	var page_arr_check = [];
	var link_arr_check = [];
	var dataStudioWebID;
	var checkArr = [];
	var check_dashbord_only_one = true;
	var data = document.getElementById("mySavedModel").value;
	localStorage.logData = data;

	var obj = JSON.parse(data);
	// console.log(obj.linkDataArray[0]);
	var configText = "dashboard:";

	try {

		for (let i = 0; i < obj.nodeDataArray.length; i++) {
			console.log(obj.nodeDataArray[i]);
			if (obj.nodeDataArray[i].category == "Comment" && (obj.nodeDataArray[i].text.split("-").length == 5 && check_dashbord_only_one == true)) {
				dataStudioWebID = obj.nodeDataArray[i].text;

				configText += dataStudioWebID + ",_,\n";
				//check dashboard ID
				dashboardID_check = dataStudioWebID;
			} else if (obj.nodeDataArray[i].category == "Comment" && (obj.nodeDataArray[i].text.split("-").length == 5 && check_dashbord_only_one == false)) {
				throw new Error("Please add only one Dashboard")
			}
			// dashboard

		}
		if (dataStudioWebID === undefined) {
			throw new Error("Please check your Dashboard ID")
		}
		for (let i = 0; i < obj.nodeDataArray.length; i++) {
			if (obj.nodeDataArray[i].text.split(":").length == 2) {
				checkArr.push(obj.nodeDataArray[i].text);
			}

		}
		//find all page
		var check_mainpage = false
		if (checkArr.length == 0) {
			throw new Error("Please add Page of Dashboard or check format value in box Page again.")


		} else {



			var page_define = uniqueArray2(checkArr);
			page_define = sort_temp_check(page_define);

			for (let i = 0; i < page_define.length; i++) {
				console.log(page_define[i])

				if (/[p|P][0-9]+[/:]\w+$/.test(page_define[i]) == false) {
					throw new Error(page_define[i] + " wrong formate Please try again.")
				}
				if (page_define[i].split(":")[0] == "P1") {
					check_mainpage = true;
				}
				configText += page_define[i] + ",_,\n";
				//page define

				//check page
				page_arr_check.push(page_define[i])

			}
			if (check_mainpage == false) {
				throw new Error("P1(main page of dashboard) is missing Please check again.")
			}






			for (let i = 0; i < obj.nodeDataArray.length; i++) {
				if (obj.nodeDataArray[i].category == "Conditional") {
					configText += "link:"
					var arrSrc = findSrc(obj, obj.nodeDataArray[i].key);
					var arrDest = findDest(obj, obj.nodeDataArray[i].key);
					if (arrSrc.length <= 0) {
						throw new Error("Please add Source page before save.")
					}

					var param = obj.nodeDataArray[i].text;

					if (/\w+[\(]\w+[\)]$/.test(param) == false) {
						throw new Error(param + " wrong format Please try again.");
					}
					configText += arrSrc[0].split(":")[0] + "->" + param + "->[";

					if (arrDest.length <= 0) {
						throw new Error("Please add Destination page before save.")
					}
					for (let j = 0; j < arrDest.length; j++) {
						if (j == arrDest.length - 1) {
							configText += arrDest[j].split(":")[0];
						} else {
							configText += arrDest[j].split(":")[0] + "&"
						}

					}
					configText += "],_,\n"

				}
			}

			for (let i = 0; i < obj.nodeDataArray.length; i++) {
				if (obj.nodeDataArray[i].category == "Start") {
					configText += "map:"
					column_name_GoogleMaps = obj.nodeDataArray[i].text;
					console.log(column_name_GoogleMaps);

					var arrDes = findDest(obj, obj.nodeDataArray[i].key);
					console.log(arrDes);
					for (let j = 0; j < arrDest.length; j++) {
						// if(j == arrDest.length - 1){
						// 	configText += arrDest[j].split(":")[0];
						// } else {
						// 	configText += arrDest[j].split(":")[0] + "&"
						// }
						var tmp_page = arrDes[j].split(":")[0];
						configText += tmp_page + "->" + column_name_GoogleMaps;
					}

					configText += ",_,\n"
				}
			}



			// write file section
			var workflow_dashboard = configText.split(",_,")[0].substring(10)


			console.log("writefilesection#" + configText);
			console.log("defaultwritefilesection#" + workflow_dashboard + "#" + data);

			closePopup()
			textInPopup(dashboardID_check, page_arr_check, configText);
		}
	} catch (e) {
		alert(e)
	}
}


function findDest(obj, key) {
	var arrKey = []; //destination
	var arrText = [];
	for (let i = 0; i < obj.linkDataArray.length; i++) {
		if (obj.linkDataArray[i].from == key) {
			arrKey.push(obj.linkDataArray[i].to)
		}

	}

	for (let i = 0; i < obj.nodeDataArray.length; i++) {
		for (let j = 0; j < arrKey.length; j++) {
			if (obj.nodeDataArray[i].key == arrKey[j]) {
				arrText.push(obj.nodeDataArray[i].text)
			}


		}

	}
	return arrText;
}

function findSrc(obj, key) {
	var arrKey = []; //source
	var arrText = [];
	for (let i = 0; i < obj.linkDataArray.length; i++) {
		if (obj.linkDataArray[i].to == key) {
			arrKey.push(obj.linkDataArray[i].from)
		}

	}

	for (let i = 0; i < obj.nodeDataArray.length; i++) {
		for (let j = 0; j < arrKey.length; j++) {
			if (obj.nodeDataArray[i].key == arrKey[j]) {
				arrText.push(obj.nodeDataArray[i].text)
			}


		}

	}
	return arrText;
}

function closePopup() {
	var obj = document.getElementById("popup_bg_color");
	var obg_item = document.getElementById("popup_container")
	if (obj.style.display === "none") {
		obj.style.display = "block";
		obg_item.style.display = "block";
	} else {
		obj.style.display = "none";
		obg_item.style.display = "none";
	}
}

function textInPopup(dashboardID_check, page_arr_check, configText) {
	document.getElementById("dashboard_ID_check").innerHTML = '';
	document.getElementById("page_id_check").innerHTML = '';
	var txt = document.getElementById("txt_check");

	document.getElementById("dashboard_ID_check").innerHTML = "Dashboard ID = " + dashboardID_check;


	var page_check = document.getElementById("page_id_check");

	for (let i = 0; i < page_arr_check.length; i++) {
		let tag_a = document.createElement('a');
		tag_a.style.margin = "4px"
		tag_a.target = '_blank';
		tag_a.style.lineHeight = "24px"
		tag_a.href = "https://datastudio.google.com/u/0/reporting/" + dashboardID_check + "/page/" + page_arr_check[i].split(":")[1];

		tag_a.innerHTML = page_arr_check[i] + "<br>"
		page_check.append(tag_a)

	}


	// txt.innerHTML = configText;
}

function sort_temp_check(pageDefine) {

	return pageDefine.sort((a, b) => {
		return parseInt(a.split(":")[0].split("P")[1]) -
			parseInt(b.split(":")[0].split("P")[1])
	});
}